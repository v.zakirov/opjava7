package org.sample;

import org.openjdk.jmh.Main;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.regex.Pattern;

public class MyBenchmark {

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    public boolean ParseInt(MyTests test) {
        try {
            Integer myInt = Integer.parseInt(test.str);
        } catch (RuntimeException e) {
            return false;
        }
        return true;

    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    public boolean IsDigit(MyTests test) {
        String sstr = test.str;
        int numChar = sstr.length();
        for (int i = 0; i < numChar; ++i) {
            if (!Character.isDigit(sstr.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    public boolean Regular(MyTests test) {
        return Pattern.matches("^\\d+$", test.str);
    }

    @State(Scope.Benchmark)
    public static class MyTests {
        @Param({  "85345"
                , "93825"
                , "43252"
                , "34f46"
                , "error"
                , "4353d"})
        public String str;
    }

    public static void main(String[] args) {
        try {
            Main.main(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
