
import java.util.Scanner;
import java.util.Random;

public class Main2 {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        Random generator = new Random();

        int random = generator.nextInt(100) + 1;
        System.out.println("I've thought a number, try to guess!");
        int guess = keyboard.nextInt();
        while (guess != random) {
            if (guess > random) {
                System.out.print("Lower");
            } else {
                System.out.print("Greater");
            }
            guess = keyboard.nextInt();
        }
        System.out.println("Exactly! Good bye!");
    }
}

/*
Code:
      stack=3, locals=5, args_size=1
         0: new           #7                  // class java/util/Scanner
         3: dup
         4: getstatic     #9                  // Field java/lang/System.in:Ljava/io/InputStream;
         7: invokespecial #15                 // Method java/util/Scanner."<init>":(Ljava/io/InputStream;)V
        10: astore_1
        11: new           #18                 // class java/util/Random
        14: dup
        15: invokespecial #20                 // Method java/util/Random."<init>":()V
        18: astore_2
        19: aload_2
        20: bipush        100
        22: invokevirtual #21                 // Method java/util/Random.nextInt:(I)I
        25: iconst_1
        26: iadd
        27: istore_3
        28: getstatic     #25                 // Field java/lang/System.out:Ljava/io/PrintStream;
        31: ldc           #29                 // String I\'ve thought a number, try to guess!
        33: invokevirtual #31                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
        36: aload_1
        37: invokevirtual #37                 // Method java/util/Scanner.nextInt:()I
        40: istore        4
        42: iload         4
        44: iload_3
        45: if_icmpeq     82
        48: iload         4
        50: iload_3
        51: if_icmple     65
        54: getstatic     #25                 // Field java/lang/System.out:Ljava/io/PrintStream;
        57: ldc           #40                 // String Lower
        59: invokevirtual #42                 // Method java/io/PrintStream.print:(Ljava/lang/String;)V
        62: goto          73
        65: getstatic     #25                 // Field java/lang/System.out:Ljava/io/PrintStream;
        68: ldc           #45                 // String Greater
        70: invokevirtual #42                 // Method java/io/PrintStream.print:(Ljava/lang/String;)V
        73: aload_1
        74: invokevirtual #37                 // Method java/util/Scanner.nextInt:()I
        77: istore        4
        79: goto          42
        82: getstatic     #25                 // Field java/lang/System.out:Ljava/io/PrintStream;
        85: ldc           #47                 // String Exactly! Good bye!
        87: invokevirtual #31                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
        90: return
 */