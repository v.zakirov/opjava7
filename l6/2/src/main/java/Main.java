import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Source s = null;
        try {
            s = Source.newBuilder("llvm", new File("cpuinfo.bc")).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Context c = Context.newBuilder().allowNativeAccess(true).build();
        Value lib = c.eval(s);
        Value fn = lib.getMember("getcpuinfo");
        fn.executeVoid();
    }
}
