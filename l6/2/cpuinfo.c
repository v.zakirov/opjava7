#include <stdio.h>

void getcpuinfo() {
	char* buf = (char*)calloc(4096, sizeof(char));
	FILE* file = fopen("/proc/cpuinfo", "r");
	if (file) {
		int num  = 0;
		do {
			num = fread(buf, sizeof(char), 4096, file);
			fwrite(buf, sizeof(char), num, stdout);
		} while (0 < num);
	}
	fclose(file);
	free(buf);
}

int main() {
	getcpuinfo();
	return 0;
}