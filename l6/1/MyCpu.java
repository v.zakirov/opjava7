public class MyCpu {
	static {
		System.loadLibrary("mycpu");
	}

	private native String getCpuInfo();

	public static void main(String[] args) {
		MyCpu myCpu = new MyCpu();
		String str = myCpu.getCpuInfo();
		String[] strArr = str.split("\n", 20);
		int index1 = str.lastIndexOf("cpu cores");
		int index2 = str.lastIndexOf("cpu MHz");
		int index3 = str.lastIndexOf("model name");
		while ('\n' != str.charAt(index1)) {
			System.out.print(str.charAt(index1++));
		}
		System.out.println();
		while ('\n' != str.charAt(index2)) {
			System.out.print(str.charAt(index2++));
		}
		System.out.println();
		while ('\n' != str.charAt(index3)) {
			System.out.print(str.charAt(index3++));
		}
		System.out.println();
	}

}