#include <jni.h>
#include <stdio.h>
#include "MyCpu.h"

JNIEXPORT jstring JNICALL Java_MyCpu_getCpuInfo(JNIEnv *env, jobject thisObj) {
	char* buf = (char*)calloc(4096, sizeof(char));
	FILE* file = fopen("/proc/cpuinfo", "r");
	if (file) {
		fread(buf, sizeof(char), 4096, file);
	}
	fclose(file);
	jstring jstrBuf = (*env)->NewStringUTF(env, buf);
	return jstrBuf;
}