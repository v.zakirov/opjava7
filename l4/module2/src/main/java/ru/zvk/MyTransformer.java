package ru.zvk;

import javassist.*;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class MyTransformer implements ClassFileTransformer {

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        if (className.equals("ru/zvk/TransactionProcessor")) {
            try {
                ClassPool classPool = ClassPool.getDefault();
                CtClass ctClass = classPool.get("ru.zvk.TransactionProcessor");

                CtField timeMin = new CtField(CtClass.longType, "timeMin", ctClass);
                timeMin.setModifiers(Modifier.STATIC);
                ctClass.addField(timeMin);

                CtField timeMax = new CtField(CtClass.longType, "timeMax", ctClass);
                timeMax.setModifiers(Modifier.STATIC);
                ctClass.addField(timeMax);

                CtField timeSum = new CtField(CtClass.longType, "timeSum", ctClass);
                timeSum.setModifiers(Modifier.STATIC);
                ctClass.addField(timeSum);

                //main method
                CtMethod mainMethod = ctClass.getDeclaredMethod("main");
                mainMethod.insertBefore(
                                "timeSum = 0;"
                );
                mainMethod.insertAfter(
                            "System.out.println(\"Min time = \" + (timeMin / 1000.0) + \" sec\");" +
                                "System.out.println(\"Max time = \" + (timeMax / 1000.0) + \" sec\");" +
                                "System.out.println(\"Average time = \" + (timeSum / 10000.0) + \" sec\");"
                );

                //processTransaction method
                CtMethod procTransMethod = ctClass.getDeclaredMethod("processTransaction");
                procTransMethod.addLocalVariable("time", CtClass.longType);
                procTransMethod.insertBefore(
                            "time = System.currentTimeMillis();" +
                                "txNum += 99;"
                );
                procTransMethod.insertAfter(
                            "time = System.currentTimeMillis() - time;" +
                                "if (0 == timeSum) {" +
                                    "timeMin = time;" +
                                    "timeMax = time;" +
                                "}" +
                                "if (time < timeMin)" +
                                    "timeMin = time;" +
                                "if (time > timeMax)" +
                                    "timeMax = time;" +
                                "timeSum += time;"
                );
                return ctClass.toBytecode();
            } catch (Exception ecx) {
                ecx.printStackTrace();
            }
        }
        return null;
    }
}
