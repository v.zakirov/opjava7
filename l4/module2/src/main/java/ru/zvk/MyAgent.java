package ru.zvk;

import java.lang.instrument.Instrumentation;

public class MyAgent {
    public static void premain(String arg, Instrumentation instrumentation) {
        instrumentation.addTransformer(new MyTransformer());
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            long numClasses = instrumentation.getAllLoadedClasses().length;
            System.out.println("Num loaded classes = " + numClasses);
        }));
    }
}
