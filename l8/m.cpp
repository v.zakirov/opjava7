#include <atomic>
#include <iostream>
#include <unistd.h>
#include <thread>

class SpinLock {
public:
	void lock() {
		while (_lock.test_and_set(std::memory_order_acquire)) {}
	}

	void unlock() {
		_lock.clear(std::memory_order_release);
	}

private:
	std::atomic_flag _lock = ATOMIC_FLAG_INIT;
};

int main() {
	SpinLock lock;
	std::thread t1([&lock]() {
		lock.lock();
		for (int i = 0; i < 3; ++i) {
			std::cout << 0 << "\n";
			sleep(1);
		}
		lock.unlock();
	});
	std::thread t2([&lock]() { 
		lock.lock();
		for (int i = 0; i < 3; ++i) {
			std::cout << 1 << "\n";
			sleep(1);
		}
		lock.unlock();
	});
	std::thread t3([&lock]() { 
		//lock.lock();
		for (int i = 0; i < 3; ++i) {
			std::cout << 3 << "\n";
			sleep(1);
		}
		//lock.unlock();
	});

	t1.join();
	t2.join();
	t3.join();

	return 0;
}