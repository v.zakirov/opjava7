#include <iostream>
#include <fstream>
#include <unordered_map>
#include <cstring>
#include <cassert>
#include <cmath>
#include <cstdint>

namespace {
	using method_ptr = void (*)(std::fstream&, std::ostream&);
} // namespace

void print_binary(char c) {
	for (int i = 0; i < 8; ++i) {
		int bit = (c & (1 << (7 - i))) >> (7 - i);
		std::cout << bit;
	}
	std::cout << std::endl;
}

void f1(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Utf8\t\t";
	char byte(0);
	std::uint16_t length(0);

	fstream.read(&byte, sizeof(char));
	length |= byte & 255;
	length <<= 8;
	fstream.read(&byte, sizeof(char));
	length |= byte & 255;
	for (int i = 0; i < length; ++i) {
		fstream.read(&byte, sizeof(char));
		ostream << byte;
	}
}

void f3(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Integer\t\t";
	char byte(0);
	int number(0);

	for (int i = 0; i < 4; ++i) {
		fstream.read(&byte, sizeof(char));
		number <<= 8;
		number |= byte & 255;
	}
	ostream << number;
}

void pp(std::uint32_t i) {
	std::cout << std::endl;
	for (int j = 0; j < 4; ++j) {
		print_binary(((char*)&i)[j]);
	}
	std::cout << std::endl;
}

void f4(std::fstream& fstream, std::ostream& ostream) {
	assert((sizeof(float) == sizeof(int)));
	ostream << " = Float\t\t";

	char byte(0);
	std::int32_t bits(0);

	for (int i = 0; i < 4; ++i) {
		fstream.read(&byte, sizeof(char));
		bits <<= 8;
		bits |= byte & 255;
	}

	int s = ((bits >> 31) == 0) ? 1 : -1;
	int e = ((bits >> 23) & 0xff);
	int m = (e == 0) ?
	          (bits & 0x7fffff) << 1 :
	          (bits & 0x7fffff) | 0x800000;

	float number_f = s * m * std::pow(2, e - 150);
	ostream << number_f << 'f';
}

void f5(std::fstream& fstream, std::ostream& ostream) {//
	ostream << " = Long\t\t";
	char byte(0);
	std::uint64_t result;
	std::uint64_t high_bytes(0);
	std::uint64_t low_bytes(0);
	
	for (int i = 0; i < 4; ++i) {
		fstream.read(&byte, sizeof(char));
		high_bytes <<= 8;
		high_bytes |= byte & 255;
	}
	for (int i = 0; i < 4; ++i) {
		fstream.read(&byte, sizeof(char));
		low_bytes <<= 8;
		low_bytes |= byte;
	}
	result = (high_bytes << 32) + low_bytes;
	std::cout << result << 'L';
}

void f6(std::fstream& fstream, std::ostream& ostream) {//
	ostream << " = Double\t\t";
	char byte(0);
	std::uint64_t bits(0);
	
	std::uint64_t high_bytes(0);
	std::uint64_t low_bytes(0);
	
	for (int i = 0; i < 4; ++i) {
		fstream.read(&byte, sizeof(char));
		high_bytes <<= 8;
		high_bytes |= byte & 255;
	}
	for (int i = 0; i < 4; ++i) {
		fstream.read(&byte, sizeof(char));
		low_bytes <<= 8;
		low_bytes |= byte & 255;
	}

	bits = (high_bytes << 32) + low_bytes;

	int s = ((bits >> 63) == 0) ? 1 : -1;
	int e = (int)((bits >> 52) & 0x7ffL);
	long m = (e == 0) ?
	           (bits & 0xfffffffffffffL) << 1 :
	           (bits & 0xfffffffffffffL) | 0x10000000000000L;
    
    double result_f = s * m * pow(2, e - 1075);

	std::cout << result_f << 'D';
}

void f7(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Class\t\t";
	char byte(0);
	int number(0);
	
	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		number <<= 8;
		number |= byte & 255;
	}

	ostream << '#' << number;
}

void f8(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = String\t\t";
	char byte(0);
	int number(0);
	
	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		number <<= 8;
		number |= byte & 255;
	}

	ostream << '#' << number;
}

void f9_10_11(std::fstream& fstream, std::ostream& ostream) {
	char byte(0);
	int class_index(0);
    int name_and_type_index(0);
	
	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		class_index <<= 8;
		class_index |= byte & 255;
	}

	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		name_and_type_index <<= 8;
		name_and_type_index |= byte & 255;
	}

	ostream << '#' << class_index << ".#" << name_and_type_index;
}

void f9(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Fieldref\t\t";
	f9_10_11(fstream, ostream);
}

void f10(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Methodref\t\t";
	f9_10_11(fstream, ostream);
}

void f11(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = InterfaceMethodref\t\t";
	f9_10_11(fstream, ostream);
}

void f12(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = NameAndType\t\t";
	char byte(0);
	int name_index(0);
    int descriptor_index(0);
	
	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		name_index <<= 8;
		name_index |= byte & 255;
	}

	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		descriptor_index <<= 8;
		descriptor_index |= byte & 255;
	}

	ostream << '#' << name_index << ":#" << descriptor_index;
}

void f15(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = MethodHandle\t\t";
	char byte(0);
	int reference_kind(0);
    int reference_index(0);
	
	fstream.read(&byte, sizeof(char));
	reference_kind <<= 8;
	reference_kind |= byte & 255;

	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		reference_index <<= 8;
		reference_index |= byte & 255;
	}

	ostream << '#' << reference_kind << "=#" << reference_index;
}

void f16(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = MethodType\t\t";
	char byte(0);
    int descriptor_index(0);
	
	fstream.read(&byte, sizeof(char));
	descriptor_index <<= 8;
	descriptor_index |= byte & 255;

	ostream << '#' << descriptor_index;
}

void f17_18(std::fstream& fstream, std::ostream& ostream) {
	char byte(0);
	int bootstrap_method_attr_index(0);
    int name_and_type_index(0);
	
	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		bootstrap_method_attr_index <<= 8;
		bootstrap_method_attr_index |= byte & 255;
	}

	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		name_and_type_index <<= 8;
		name_and_type_index |= byte & 255;
	}

	ostream << '#' << bootstrap_method_attr_index << "+#" << name_and_type_index;
}

void f17(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Dynamic\t\t";
	f17_18(fstream, ostream);
}

void f18(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = InvokeDynamic\t\t";
	f17_18(fstream, ostream);
}

void f19(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Module\t\t";
	char byte(0);
	int name_index(0);
	
	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		name_index <<= 8;
		name_index |= byte & 255;
	}

	ostream << '#' << name_index;
}

void f20(std::fstream& fstream, std::ostream& ostream) {
	ostream << " = Package\t\t";
	char byte(0);
	int name_index(0);
	
	for (int i = 0; i < 2; ++i) {
		fstream.read(&byte, sizeof(char));
		name_index <<= 8;
		name_index |= byte & 255;
	}

	ostream << '#' << name_index;
}

int main(int argc, char const *argv[]) {
	if (2 != argc) {
		std::cerr << "Incorrect number of input params\n";
		return -1;
	}
	std::fstream f(argv[1], std::ios::in | std::ios::binary);
	if (!f.is_open()) {
		std::cerr << "File with name \"" << argv[1] << "\" dosn't exist\n";
		return -2; 
	}

	char byte(0);

	int start_sec[4] {0xca, 0xfe, 0xba, 0xbe};

	for (int i = 0; i < 4; ++i) {
		f.read(&byte, sizeof(char));
		if (byte != (char)start_sec[i]) {
			std::cerr << "It's not a class file\n";
			return -4;
		}
	}

	std::uint16_t minor_version(0);
	f.read((char*)&minor_version, sizeof(char));
	f.read((char*)&minor_version + 1, sizeof(char));
	std::uint16_t major_version(0);
	f.read((char*)&major_version + 1, sizeof(char));
	f.read((char*)&major_version + 0, sizeof(char));

	std::cout << "minor_version = " << minor_version << std::endl;
	std::cout << "major_version = " << major_version << std::endl;

	int num_const(0);
	f.read(&byte, sizeof(char));
	num_const |= byte & 255;
	num_const <<= 8;
	f.read(&byte, sizeof(char));
	num_const |= byte & 255;

	std::unordered_map<int, method_ptr> map;
	map.reserve(50);
	map[1] = f1;
	map[3] = f3;
	map[4] = f4;
	map[5] = f5;
	map[6] = f6;
	map[7] = f7;
	map[8] = f8;
	map[9] = f9;
	map[10] = f10;
	map[11] = f11;
	map[12] = f12;
	map[15] = f15;
	map[16] = f16;
	map[17] = f17;
	map[18] = f18;
	map[19] = f19;
	map[20] = f20;

	char tag(0);
	for (int i = 1; i < num_const; ++i) {
		if (!f.read(&tag, sizeof(char))) {
			std::cout << "Some error\n";
			return -3;
		}
		if (map.end() == map.find(tag)) {
			continue;
		}
		std::cout << '#' << i;
		map[tag](f, std::cout);
		std::cout << std::endl;
	}

	return 0;
}