import java.lang.reflect.Method;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {

    private static Method getMethodWithName(Method[] methods, String methodName) {
        for (Method method : methods) {
            if (methodName.equals(method.getName()) && method.getReturnType().toString().endsWith("java.lang.String"))
                return method;
        }
        return null;
    }

    private static void callMethod(String dirName, String methodName) {
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(Path.of(dirName))) {
            MyClassLoader myClassLoader = new MyClassLoader(dirName);
            for (Path fileName: dirStream) {
                String currentFileName = fileName.getFileName().toString();
                if (!currentFileName.endsWith(".class")) {
                    System.err.println("File with name \'" + currentFileName + "\' of the wrong type");
                    continue;
                }

                Class<?> loadClass = myClassLoader.loadClass(currentFileName);

                Method method = getMethodWithName(loadClass.getDeclaredMethods(), methodName);

                if (null == method) {
                    System.err.println("Class file with name\'" + currentFileName + "\' doesn't have a specified method");
                    continue;
                }

                Object object = loadClass.getConstructor().newInstance();
                method.setAccessible(true);
                Object result = method.invoke(object);
                System.out.println("Class name: " + loadClass.getCanonicalName());
                System.out.println("Result: " + result.toString());
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (1 != args.length) {
            System.err.println("Incorrect number of input arguments");
        }

        callMethod(args[0], "getSecurityMessage");
    }
}