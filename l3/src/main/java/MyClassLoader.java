import java.io.*;
import java.nio.file.Path;

public class MyClassLoader extends ClassLoader {

    private final String dir;

    MyClassLoader(String dir) {
        this.dir = dir;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        File file = new File(String.valueOf(Path.of(dir, name)));
        if(!file.isFile())
            throw new ClassNotFoundException("Class not found " + name);

        try (InputStream ins = new BufferedInputStream(new FileInputStream(file))) {
            byte[] b = new byte[(int)file.length()];
            ins.read(b);
            return defineClass(null, b, 0, b.length);
        } catch (Exception e){
            e.printStackTrace();
            throw new ClassNotFoundException("Class not found " + name);
        }
    }
}